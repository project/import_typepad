Import TypePad export into Drupal
Nigel Sim <nigel.sim@gmail.com>

Instructions
-----------------------------------
   1. Download your type pad export
   2. Upload the export file to the server (optional)
   3. Download the latest release
   4. gunzip it in the DRUPAL/modules directory
   5. Log in to drupal as admin
   6. Goto admin/modules and activate the import_tp module
   7. Goto admin -> content -> type pad
   8. Select the file from the local machine, or put in the location on the server. Click next
   9. See the preview. At the bottom of the page there is a list of distinct bloggers. Select the local user to map to each of them. Also select mappings for categories, as well as setting up substitutions, such as URLs for images.
  10. Click next... wait
  11. DONE!
